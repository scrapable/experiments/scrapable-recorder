const fs = require("fs")
const { exec } = require("child_process")

process.chdir("optimal-select")
exec("npm run build", err => {
	if(err) {
		console.log(err)
		process.exit(1)
	}

	const optimalSelectSrc = fs.readFileSync("dist/optimal-select.min.js")
	fs.writeFileSync("../optimal-select.js", `module.exports = () => { ${optimalSelectSrc} \n}`)
})
