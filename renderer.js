const { render, h } = require("preact")
const { useState, useEffect } = require("preact/hooks")
const htm = require("htm")

const puppeteer = require("puppeteer-core")
const chromeLauncher = require("chrome-launcher")
const fs = require("fs")

const optimalSelect = require("./optimal-select")
const createEventEmitter = require("./event-emitter")


const RENDER_MARKER = "// <<< SCRAPABLE"


const init = async () => {
	initUI()
}

const initUI = () => {
	const html = htm.bind(h)

	const ui = function() {
		const [browser, setBrowser] = useState(null)
		const [browserShouldBeActive, setBrowserShouldBeActive] = useState(false)

		const browserIsActive = browser !== null
		const browserIsLoading = browserIsActive !== browserShouldBeActive

		const [ outputFile, setOutputFile ] = useState(null)

		useEffect(async () => {
			if(browserIsLoading) {
				if(browserShouldBeActive) {
					const browser = await launchBrowser(() => outputFile)
					if(browser) {
						browser.on("browser-disconnected", () => {
							setBrowserShouldBeActive(false)
						})
					}
					setBrowser(browser)
				}
				else {
					try {
						await browser.stop()
					} catch {}
					setBrowser(null)
				}
			}
		}, [browser, browserShouldBeActive])


		const handleFileChange = evt => {
			const files = evt.currentTarget.files
			if(files.length > 0)
				setOutputFile(files[0].path)
		}


		return html`
			<input type="file" accept="text/*" onChange=${handleFileChange} />
			<button onClick=${() => setBrowserShouldBeActive(a => !a)}
				disabled=${browserIsLoading}>
				${browserIsActive ? "Stop" : "Launch"} Browser
			</button>
			<button>Record</button>
			<button>Play</button>`
	}

	render(html`<${ui} />`, document.getElementById("preact-root"))
}

const launchBrowser = async getOutputFile => {
	const browserProc = await chromeLauncher.launch()
	const browser = await puppeteer.connect({
		browserURL: `http://localhost:${browserProc.port}/json`
	})
	
	const stop = async () => {
		if(browser)
			await browser.close()
		if(browserProc)
			await browserProc.kill()
		removeEventListener("beforeunload", stop)
	}
	addEventListener("beforeunload", stop)

	const eventEmitter = await setUpGlobalEventListeners(browser, getOutputFile)

	const page = await browser.newPage()
	await page.goto("https://duckduckgo.com")

	return {
		...eventEmitter,
		stop,
		browser,
	}
}

/**
 * @param {puppeteer.Browser} browser 
 */
const setUpGlobalEventListeners = async (browser, getOutputFile) => {
	const {
		fireEvent,
		on,
		unregister,
		waitForEvent,
	} = createEventEmitter()

	const _flush = t => flush(t, getOutputFile)
	await _flush("const browser = await puppeteer.launch()\nconst pages = []")

	let highestPageIndex = 0

	browser.on("targetcreated", async target => {
		if(target.type() !== "page") return
		const page = await target.page()

		const puppeteerPageIndex = (await browser.pages()).indexOf(page)
		const pageIndex = highestPageIndex++
		await _flush(`pages.push(browser.pages()[${puppeteerPageIndex}])`)

		const pagePrefix = `pages[${pageIndex}]`
		const escapeStr = str => str.replace(/"/g, "\\\"")

		await setUpPageEventListeners(page, async evt => {
			switch(evt.type) {
				case "click":
					if(evt.extraData.targetTag === "INPUT" && evt.extraData.targetType === "checkbox")
						break
					await _flush(`await ${pagePrefix}.click("${escapeStr(evt.event.target)}")`)
					break
				case "change":
					switch(evt.extraData.targetType) {
						case "text":
						case "password":
							await _flush(`await ${pagePrefix}.type("${escapeStr(evt.extraData.value)}")`)
							break
						case "select":
							await _flush(`await ${pagePrefix}.select("${escapeStr(evt.extraData.value)}")`)
							break
						case "checkbox":
							await _flush(`;await (async () => {
const isChecked = await ${pagePrefix}.$eval("${escapeStr(evt.event.target)}", c => c.checked)
const shouldBeChecked = ${evt.extraData.checked}
if(isChecked !== shouldBeChecked)
	await ${pagePrefix}.click("${escapeStr(evt.event.target)}")
})()`)
							break
					}
					break
			}
		})

		page.on("close", () =>
			_flush(`await ${pagePrefix}.close()`))
	})

	browser.on("disconnected", async () => {
		await Promise.all([
			fireEvent("browser-disconnected"),
			_flush("await browser.close()")
		])
	})

	return {
		on,
		unregister,
		waitForEvent,
	}
}

const flush = async (text, getOutputFile) => {
	const filePath = getOutputFile()
	if(filePath == null) return

	const fileText = await new Promise((res, rej) => {
		fs.readFile(filePath, { encoding: "utf-8" }, (err, data) => {
			if(err) rej(err)
			else res(data)
		})
	})
	const renderedText = fileText.includes(RENDER_MARKER)
		? fileText.replace(RENDER_MARKER, `${text}\n${RENDER_MARKER}`)
		: `${fileText}\n${text}`
	await new Promise(r => fs.writeFile(filePath, renderedText, r))
}

/**
 * @param {puppeteer.Page} page 
 */
const setUpPageEventListeners = async (page, fireDOMEvent) => {
	const spyFnName = `_el${Date.now()}`
	await page.exposeFunction(spyFnName, fireDOMEvent)

	const evaluate = evaluateFn =>
		page[evaluateFn](setupDocumentEventListeners(spyFnName))
	
	await Promise.all([
		evaluate("evaluate"),
		evaluate("evaluateOnNewDocument")
	])
}

const setupDocumentEventListeners = spyFnName => `(${() => {
	(optimalSelect)()
	const OptimalSelect = window.OptimalSelect
	delete window.OptimalSelect

	const spyFn = window[spyFnName]

	const mapEventToSerializable = evt => {
		const tranformed = {}
		for (const key in evt) {
			const val = evt[key]
			switch(typeof val) {
				case "function":
				case "symbol":
					continue
				case "object":
					if(val instanceof HTMLElement)
						tranformed[key] = OptimalSelect.select(val)
					break
				default:
					tranformed[key] = val
			}
		}
		return tranformed
	}

	const listen = (evtName, extractExtraData = () => undefined) => {
		document.addEventListener(evtName, evt => {
			spyFn({
				type: evtName,
				event: mapEventToSerializable(evt),
				extraData: extractExtraData(evt),
			})
		}, {
			capture: true,
			passive: true,
		})
	}

		listen("click", evt => ({
			targetTag: evt.target.tagName,
			targetType: evt.target.type,
		}))
		listen("change", evt => ({
			value: evt.target.value,
			checked: evt.target.checked,
			targetType: evt.target.type,
		}))

}})()`.replace("optimalSelect", optimalSelect.toString())
	.replace("spyFnName", `"${spyFnName}"`)

addEventListener("load", init)
