const { app, BrowserWindow } = require("electron")

// Keep a global reference of the window object, if you don"t, the window will
// be closed automatically when the JavaScript object is garbage collected.
let globalMainWindow

const createMainWindow = () => {
	const mainWindow = new BrowserWindow({
		width: 300,
		height: 100,
		resizable: false,
		webPreferences: {
			nodeIntegration: true,
		},
		alwaysOnTop: true,
	})

	mainWindow.loadFile("index.html")
	mainWindow.setMenuBarVisibility(false)

	mainWindow.webContents.openDevTools({
		mode: "detach",
	})

	globalMainWindow = mainWindow
}

app.on("ready", createMainWindow)

app.on("window-all-closed", () => {
	// On macOS it is common for applications and their menu bar
	// to stay active until the user quits explicitly with Cmd + Q
	if(process.platform !== "darwin") {
		app.quit()
	}
})

app.on("activate", () => {
	// On macOS it"s common to re-create a window in the app when the
	// dock icon is clicked and there are no other windows open.
	if(globalMainWindow === null) {
		createMainWindow()
	}
})
